package com.accounts.animalcrossing;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public int total; // total is positive if Alois owes money to Alban (Alois has more money)
    public int undototal;
    public int redototal;
    private Switch virementSwitch;
    private CheckBox albanCheckbox;
    private CheckBox aloisCheckbox;
    private EditText amountInput;
    private TextView summaryText;
    private Button validateButton;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initiate TextView
        summaryText = (TextView) findViewById(R.id.textSummary);

        // initiate CheckBoxes
        albanCheckbox = (CheckBox) findViewById(R.id.checkBoxAlban);
        aloisCheckbox = (CheckBox) findViewById(R.id.checkBoxAlois);

        // initiate Switch
        virementSwitch = (Switch) findViewById(R.id.switchVirement);

        // initiate EditText
        amountInput = (EditText) findViewById(R.id.montantInput);

        // shared Preferences
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        total = sharedPref.getInt(getString(R.string.amountTotal), 0);
        undototal = total;
        redototal = total;
        setSummaryText();
    }

    public void onClickButton(View v) {
        int amount = getInputValue();
        computeTotal(amount);

        // change text summary
        setSummaryText();

        // change shared preference total
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.amountTotal), total);
        editor.commit();

        // reset all
        albanCheckbox.setChecked(false);
        aloisCheckbox.setChecked(false);
        amountInput.setText(null);
        virementSwitch.setChecked(false);
        hideKeyboard(this);
    }

    public void onClickPlusHundredButton(View v) {
        int amount = getInputValue();
        amount += 100;
        amountInput.setText(""+amount, TextView.BufferType.EDITABLE);
    }

    private int getInputValue() {
        int amount;
        if(isEmpty(amountInput)) {
            amount = 0;
        } else {
            amount = Integer.parseInt(amountInput.getText().toString());
        }
        return amount;
    }

    private void computeTotal(int amount) {
        // get current state of the Switch
        Boolean virementSwitchChecked = virementSwitch.isChecked();
        if(!virementSwitchChecked) {
            if(albanCheckbox.isChecked() && !aloisCheckbox.isChecked()) {
                updateUndoTotal();
                total -= amount;
                updateRedoTotal();
            } else if (aloisCheckbox.isChecked() && !albanCheckbox.isChecked()) {
                updateUndoTotal();
                total += amount;
                updateRedoTotal();
            }
        } else {
            if(aloisCheckbox.isChecked() && !albanCheckbox.isChecked()) {
                updateUndoTotal();
                total -= 2*amount;
                updateRedoTotal();
            } else if (albanCheckbox.isChecked() && !aloisCheckbox.isChecked()) {
                updateUndoTotal();
                total += 2*amount;
                updateRedoTotal();
            }
        }
    }

    private void setSummaryText(){
        int debt;
        if(total/2==0) {
            summaryText.setText("Tout est beau !");
        } else if (total > 0) { // Alois has more money
            debt = total/2;
            if(debt==1) {
                summaryText.setText("Aloïs doit " + debt + " clochette à Alban !");
            } else {
                summaryText.setText("Aloïs doit " + debt + " clochettes à Alban !");
            }
        } else {  // Alban has more money
            debt = Math.abs(total)/2;
            if(debt==1) {
                summaryText.setText("Alban doit " + debt + " clochette à Aloïs !");
            } else {
                summaryText.setText("Alban doit " + debt + " clochettes à Aloïs !");
            }
        }
    }

    private boolean isEmpty(EditText eText) {
        return eText.getText().toString().trim().length() == 0;
    }

    private void updateUndoTotal() {
        undototal = total;
    }

    private void updateRedoTotal() {
        redototal = total;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cancel_app_bar, menu);
        return true;
    }

    public void onUndoClick(MenuItem i) {
        total = undototal;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.amountTotal), total);
        editor.commit();

        setSummaryText();
    }

    public void onRedoClick(MenuItem i) {
        total = redototal;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.amountTotal), total);
        editor.commit();

        setSummaryText();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {}


}
